import React from 'react';
import _ from 'lodash';

const UserFollowers = (props) => {
	const formatCards = () => {
		const data  = props.location ? props.location.state.data : props.data;
		const _type = props.location ? props.location.state._type : props._type;
		if(!data.length) return `No ${_type}`;
		
		return _.map(data, (item, ind) => {
			const images = _.values(item.images);
			return (
					<div key={ ind } className='col-sm-3 mt-4  mb-4 card-set'>
						<div className="card">
							<a href={ item.url } className='plain-link' target='_blank'>
								<img className="card-img-top" src={ images[ images.length - 1 ] }
										 alt={ `${_type} image` }/>
								<div className="card-footer">
									<small><p className='truncate'>{ item.display_name }</p></small>
								</div>
							</a>
						</div>
					</div>
			);
		});
	};
	
	return (
			<div className="d-flex flex-wrap">
				<div className="card-deck">
					{ formatCards() }
				</div>
			</div>
	);
};

export default UserFollowers;