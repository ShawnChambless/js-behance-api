import React from 'react';
import _ from 'lodash';

const UserInfo = ({ user }) => {
	
	const formatFields = () => {
		return user.fields.join(' | ');
	};
	
	const formatStats = () => {
		const stats = user.stats;
		return (
				<div>
					<span><span className="oi oi-star"/> { stats.following }</span>&nbsp;&nbsp;
					<span><span className="oi oi-people"/> { stats.followers }</span>&nbsp;&nbsp;
					<span><span className="oi oi-heart"/> { stats.appreciations }</span>&nbsp;&nbsp;
					<span><span className="oi oi-eye"/> { stats.views }</span>&nbsp;&nbsp;
					<span><span className="oi oi-comment-square"/> { stats.comments }</span>
				</div>
		);
	};
	const userImages  = _.values(user.images);
	
	return (
			<div style={ { width: '100%' } }>
				<div className='d-flex align-items-center justify-content-start flex-wrap-reverse'>
					<div>
						<div className='d-flex flex-column  align-items-center user-image-stats'>
							<img style={ { minWidth: '300px', marginBottom: '15px' } } src={ userImages[ userImages.length - 1 ] }
									 alt=""/>
							<div className='d-flex flex-column user-stats-wrapper'>
								{ !user.location ? null :
										<div className='user-stats'>{ user.location }</div>
								}
								<div className='user-stats'>{ formatFields() }</div>
								<div className='user-stats'>{ formatStats() }</div>
							</div>
						</div>
					</div>
					<div className='user-headline-name'>
						<p>{ user.display_name }</p>
						{ !user.occupation ? null : <small className='text-muted'>{ user.occupation }</small> }
						{ user.workExperience.length ?
								<div style={ { marginTop: '-5px' } }>
									<small
											style={ { fontSize: '.75em' } }>{ 'Previously: ' + _.map(user.workExperience.splice(0, 3), item => ` ${item.position}`) }</small>
								</div> : null }
					</div>
				</div>
			</div>
	);
};

export default UserInfo;