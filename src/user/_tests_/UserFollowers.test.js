import React from 'react';
import ReactDOM from 'react-dom';
import UserFollowers from '../UserFollowers';
import { configure, shallow } from 'enzyme';
import { user } from './UserDataMock';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
describe('UserFollowers', () => {
	const data  = [];
	const _type = 'followers';
	
	it('Should render without breaking', () => {
		const div = document.createElement('div');
		ReactDOM.render(<UserFollowers _type={ _type } data={ data }/>, div);
	});
	
	it('Should render set the key prop to the index', () => {
		const data      = [ user ];
		const component = shallow(<UserFollowers _type='followers' data={ data }/>);
		expect(component.find('div').at(2).key('key')).toEqual('0');
	});
	
	it('Should render render 3 parent divs', () => {
		const data      = [ user, user, user ];
		const component = shallow(<UserFollowers _type='followers' data={ data }/>);
		expect(component.findWhere(node => node.key('key') !== undefined).length).toEqual(3);
	});
	
	it('Should return "No followers"', () => {
		const component = shallow(<UserFollowers _type='followers' data={ [] }/>);
		expect(component.containsMatchingElement(<div className="card-deck">No followers</div>));
	});
	
});
