export const user = [ {
	'id':           50001,
	'location':     'Brooklyn, NY, USA',
	'occupation':   'Co-Founder of Behance',
	'url':          'https://www.behance.net/MatiasCorea',
	'images':       {
		'50':  'https://mir-s3-cdn-cf.behance.net/user/50/aa9a5250001.564c90b1c8c1c.jpg',
		'100': 'https://mir-s3-cdn-cf.behance.net/user/100/aa9a5250001.564c90b1c8c1c.jpg',
		'115': 'https://mir-s3-cdn-cf.behance.net/user/115/aa9a5250001.564c90b1c8c1c.jpg',
		'138': 'https://mir-s3-cdn-cf.behance.net/user/138/aa9a5250001.564c90b1c8c1c.jpg',
		'230': 'https://mir-s3-cdn-cf.behance.net/user/230/aa9a5250001.564c90b1c8c1c.jpg',
		'276': 'https://mir-s3-cdn-cf.behance.net/user/276/aa9a5250001.564c90b1c8c1c.jpg'
	},
	'display_name': 'Matias Corea',
	'fields':       [
		'Graphic Design',
		'Print Design',
		'Interaction Design'
	],
	'stats':        {
		'followers':     76946,
		'following':     1347,
		'appreciations': 61776,
		'views':         1085963,
		'comments':      423,
		'team_members':  false
	}
}
];