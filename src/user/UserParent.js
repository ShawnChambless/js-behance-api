import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import UserInfo from './UserInfo';
import UserProjects from './UserProjects';
import UserFollowers from './UserFollowers';
import SearchParent from '../search/SearchParent';
import { connect } from 'react-redux';
import { getUserFromAPI } from '../actions';
import _ from 'lodash';

class UserParent extends Component {
	
	constructor(props) {
		super();
		this.state = { id: props.match ? props.match.params.id : props.id };
	}
	
	componentDidMount() {
		this.props.getUserFromAPI(this.state.id);
	}
	
	render() {
		const loadingUser = !_.has(this.props.user, 'projects') && !_.has(this.props.user, 'followers') && !_.has(this.props.user, 'followers');
		if(loadingUser) return 'Loading user info...';
		else
			return (
					<div>
						<SearchParent/>
						<div style={ { marginTop: '2em' } }>
							<div className="d-flex justify-content-start">
								<UserInfo user={ this.props.user }/>
							</div>
							<div className="d-flex flex-column align-items-between user-links">
								<div className='link-set'>
									<h3>Projects
										<Link
												to={ { pathname: `/user/${this.props.user.id}/projects`, state: this.props.user.projects } }>
											See all
										</Link>
									</h3>
									<UserProjects projects={ this.props.user.projects.slice(0, 4) }/>
								</div>
								<div className='link-set'>
									<h3>Followers
										<Link to={ {
											pathname: `/user/${this.props.user.id}/Followers`,
											state:    { _type: 'followers', data: this.props.user.followers }
										} }>
											See all
										</Link>
									</h3>
									<UserFollowers _type='followers' data={ this.props.user.followers.slice(0, 4) }/>
								</div>
								<div className='link-set'>
									<h3>Following
										<Link to={ {
											pathname: `/user/${this.props.user.id}/Following`,
											state:    { _type: 'following', data: this.props.user.following }
										} }>
											See all
										</Link>
									</h3>
									<UserFollowers _type='following' data={ this.props.user.following.slice(0, 4) }/>
								</div>
							</div>
						</div>
					</div>
			);
	}
}

const mapUserToProps = ({ users: { user } }) => {
	return {
		user: user
	};
};

export default connect(mapUserToProps, { getUserFromAPI })(UserParent);