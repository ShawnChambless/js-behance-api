import React, { Component } from 'react';
import _ from 'lodash';

class UserProjects extends Component {
	formatProjects() {
		const projects = this.props.location ? this.props.location.state : this.props.projects;
		if(!projects) return 'No projects';
		return _.map(projects, (project, ind) => {
			const images = _.values(project.covers);
			return (
					<div key={ ind } className='col-sm-3 mt-4 mb-4 card-set'>
						<div className="card">
							<a href={ project.url } className='plain-link' target='_blank'>
								<img className="card-img-top" src={ images[ images.length - 2 ] }
										 alt="Project Cover"/>
								<div className="card-footer">
									<small><p className='truncate'>{ project.name }</p></small>
								</div>
							</a>
						</div>
					</div>
			);
		});
	};
	
	render() {
		return (
				<div className="d-flex flex-wrap justify-content-between">
					<div className="card-deck">
						{ this.formatProjects() }
					</div>
				</div>
		);
	}
}

export default UserProjects;