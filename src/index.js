import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import './assets/open-iconic-master/font/css/open-iconic-bootstrap.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route } from 'react-router-dom';
import UserParent from './user/UserParent';
import UserFollowers from './user/UserFollowers';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import reducers from './reducers';
import promise from 'redux-promise';
import UserProjects from './user/UserProjects';
import SearchParent from './search/SearchParent';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
		<Provider store={ createStoreWithMiddleware(reducers) }>
			<div>
				<BrowserRouter>
					<div>
						<Route exact path='/' render={ () => <SearchParent marginTop='10%' title='Search for a user'/> }/>
						<Route exact path='/user/:id' component={ UserParent }/>
						<Route path='/user/:id/projects' component={ UserProjects }/>
						<Route path='/user/:id/followers' component={ UserFollowers }/>
						<Route path='/user/:id/following' component={ UserFollowers }/>
					</div>
				</BrowserRouter>
			</div>
		</Provider>, document.getElementById('root'));
registerServiceWorker();
