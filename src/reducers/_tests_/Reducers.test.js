import Reducer from '../user_reducer';
import { FETCH_USERS, GET_USER_FROM_API } from '../../actions';

describe('reducers', () => {
	it('should return the initial state', () => {
		expect(Reducer(undefined, {})).toEqual({
			showResults: false
		});
	});
	
	it('should handle FETCH_USERS', () => {
		expect(Reducer({}, { type: FETCH_USERS, payload: { data: [ 1 ] } }))
				.toEqual({
					showResults: true
					, users:     [ 1 ]
				});
	});
	
	it('should handle GET_USER_FROM_API', () => {
		expect(Reducer({}, { type: GET_USER_FROM_API, payload: { data: [ 1 ] } }))
				.toEqual({
					showResults: false
					, user:      [ 1 ]
				});
	});
});