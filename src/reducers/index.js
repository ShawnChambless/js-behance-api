import { combineReducers } from 'redux';
import UsersReducer from './user_reducer';

const rootReducer = combineReducers({
	users: UsersReducer
});

export default rootReducer;
