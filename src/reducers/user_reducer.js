import { FETCH_USERS, GET_USER_FROM_API } from '../actions';

export default function(state = { showResults: false }, action) {
	switch(action.type) {
		case FETCH_USERS:
			return Object.assign({}, state, { users: action.payload.data, showResults: true });
		case GET_USER_FROM_API:
			return Object.assign({}, state, { user: action.payload.data, showResults: false });
		default:
			return state;
	}
}