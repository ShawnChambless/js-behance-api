import axios from 'axios';

export const FETCH_USERS = 'FETCH_USERS'
		, SET_USER           = 'SET_USER'
		, GET_USER_FROM_API  = 'GET_USER_FROM_API';

export const fetchUsers = (searchTerm) => {
	const request = axios.get(`http://localhost:8080/fetchUsers/${searchTerm}`);
	return {
		type:      FETCH_USERS
		, payload: request
	};
};

export const setUser = (id) => {
	return {
		type:      SET_USER
		, payload: id
	};
};

export const getUserFromAPI = (id) => {
	const request = axios.get(`http://localhost:8080/user/${id}`);
	
	return {
		type:      GET_USER_FROM_API
		, payload: request
	};
};