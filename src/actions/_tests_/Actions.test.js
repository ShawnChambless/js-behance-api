import * as Actions from '../index';


describe('actions', () => {
	it('should create a setUser action', () => {
		const id             = 11111;
		const expectedAction = {
			type:      Actions.SET_USER
			, payload: id
		};
		expect(Actions.setUser(id)).toEqual(expectedAction);
	});
});
