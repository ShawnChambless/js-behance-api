import React from 'react';

const SearchBar = (props) => {
	return (
			<div>
				<div className="input-group">
					<input type="text" id='user-search' onChange={ (e) => {
						e.persist();
						props.updateSearchQuery(e);
					} }
								 className="form-control"
								 placeholder="John Doe"/>
				</div>
			</div>
	);
};

export default SearchBar;