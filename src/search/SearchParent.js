import React, { Component } from 'react';
import SearchBar from './SearchBar';
import SearchResults from './SearchResults';
import { fetchUsers, getUserFromAPI } from '../actions';
import _ from 'lodash';
import { connect } from 'react-redux';

class SearchParent extends Component {
	
	constructor() {
		super();
		this.state       = { users: [], searchTerm: '' };
		this.inputChange = this.inputChange.bind(this);
	}
	
	inputChange({ target: { value } }) {
		this.props.fetchUsers(value);
	}
	
	render() {
		const userSearch = _.debounce((term) => this.inputChange(term), 300);
		return (
				<div style={ { marginTop: this.props.marginTop || '2em' } }>
					{ this.props.title ? <h4>{ this.props.title }</h4> : null }
					<SearchBar updateSearchQuery={ userSearch }/>
					{ !this.props.showResults || !this.props.users ? null :
							<SearchResults users={ this.props.users }/>
					}
				</div>
		);
	}
}

const mapStateToProps = ({ users }) => {
	return {
		users:         users.users
		, user:        users.user
		, showResults: users.showResults || false
	};
};

export default connect(mapStateToProps, { fetchUsers })(
		connect(mapStateToProps, { getUserFromAPI })(SearchParent));