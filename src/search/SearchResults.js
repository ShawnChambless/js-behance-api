import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getUserFromAPI } from '../actions';

class SearchResults extends Component {
	
	renderUsersList() {
		return this.props.users.map((user, ind) => {
			return (
					<Link key={ ind } onClick={ this.props.getUserFromAPI.bind(this, user.id) } className='plain-link'
								to={ { pathname: `/user/${user.id}`, state: { user } } }>
						<li className="list-group-item result-list">
							<div className="d-flex justify-content-start align-items-start result-box">
								<img src={ user.images[ '115' ] } alt='User Image'/>
								<div className="d-flex flex-column justify-content-center result-text">
									<h3 style={{marginBottom: 0}}>{ user.display_name }</h3>
									<p>{ user.fields.join(' | ') }</p>
								</div>
							</div>
						</li>
					</Link>
			);
		});
	};
	
	render() {
		
		return (
				<div>
					<ul className="list-group">
						{ this.props.users.length ? this.renderUsersList() : '' }
					</ul>
				</div>
		);
	}
}

export default connect(null, { getUserFromAPI })(SearchResults);
