const express    = require('express')
		, app        = express()
		, port       = 8080
		, bodyParser = require('body-parser')
		, axios      = require('axios')
		, cache      = require('memory-cache')
		, _          = require('lodash');

const axiosInstance = axios.create({
	baseURL:  'https://api.behance.net/v2/users',
	timeout:  10000
	, params: {
		api_key: 'GmZHeRKTDGrGWoD0P3TgDHW6WyWu1IQE'
	}
});

app
		.use(bodyParser.json())
		.use((req, res, next) => {
			res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
			next();
		});

app.get('/fetchUsers/:query', (req, res) => {
	axiosInstance.get(`?q=${req.params.query}`)
			.catch(err => console.log(`Error fetching users\n\n${err}`))
			.then(users => {
				cache.put(req.params.query, users.data.users);
				return res.status(200).json(users.data.users);
			});
});

app.get('/user/:user_id', (req, res) => {
	const cacheTry = cache.get(req.params.user_id);
	if(cacheTry) return res.status(200).json(cacheTry);
	
	const getUserInfo           = axiosInstance.get(`/${req.params.user_id}`);
	const getUserProjects       = axiosInstance.get(`/${req.params.user_id}/projects`);
	const getUserFollowers      = axiosInstance.get(`/${req.params.user_id}/followers`);
	const getUserFollowing      = axiosInstance.get(`/${req.params.user_id}/following`);
	const getUserWorkExperience = axiosInstance.get(`/${req.params.user_id}/work_experience`);
	
	return axios.all([ getUserInfo, getUserProjects, getUserFollowers, getUserFollowing, getUserWorkExperience ])
			.then(axios.spread((userInfo, userProjects, userFollowers, userFollowing, userWorkExperience) => {
				const userWithData = {
					...userInfo.data.user,
					projects:       userProjects.data.projects,
					followers:      userFollowers.data.followers,
					following:      userFollowing.data.following,
					workExperience: userWorkExperience.data.work_experience
				};
				
				cache.put(req.params.user_id, userWithData);
				return res.status(200).json(userWithData);
			}));
});

app.get('/user/:user_id/projects', (req, res) => {
	const cacheTry = cache.get(`${req.params.user_id}/projects`);
	if(cacheTry) return res.status(200).json(cacheTry);
	
	return axiosInstance.get(`/${req.params.user_id}/projects`)
			.then(userProjects => {
				const { data: { projects } } = userProjects;
				cache.put(`${req.params.user_id}/projects`, projects);
				return res.status(200).json(projects);
			});
});

app.get('/user/:user_id/followers', (req, res) => {
	const cacheTry = cache.get(`${req.params.user_id}/followers`);
	if(cacheTry) return res.status(200).json(cacheTry);
	
	return axiosInstance.get(`/${req.params.user_id}/followers`)
			.then(userProjects => {
				const { data: { followers } } = userProjects;
				cache.put(`${req.params.user_id}/followers`, followers);
				return res.status(200).json(followers);
			});
});

app.get('/user/:user_id/following', (req, res) => {
	const cacheTry = cache.get(`${req.params.user_id}/following`);
	if(cacheTry) return res.status(200).json(cacheTry);
	
	return axiosInstance.get(`/${req.params.user_id}/following`)
			.then(userProjects => {
				const { data: { following } } = userProjects;
				cache.put(`${req.params.user_id}/following`, following);
				return res.status(200).json(following);
			});
});

app.listen(port, () => console.log(`listening on ${port}`));
